<?php

/**
 * @file
 * A single location to store configuration.
 */

define('TWEET_POSTER_CONSUMER_KEY', variable_get('tweet_poster_consumer_key', NULL));
define('TWEET_POSTER_CONSUMER_SECRET', variable_get('tweet_poster_consumer_secret', NULL));
define('TWEET_POSTER_OAUTH_CALLBACK', variable_get('tweet_poster_callback_url', NULL));
define('TWEET_POSTER_TWEETPIC_KEY', variable_get('tweet_poster_tweetpic_key', NULL));
