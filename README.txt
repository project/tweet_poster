Tweet Poster Module
======================

Dependencies - libraries.

Libraries required.
1. twitteroauth. 
2. tmhOAuth (0.7.2).
---------------------------------

1. How to configure tweet poster
------------------------------
Below are the steps to configure.

1. Create a twitter app for your Website or project (https://dev.twitter.com/).
2. Download the module.
3. Enable it and go to admin > Twitter API Credential Settings.
   Put Twitter App Consumer Key.
   Twitter App Consumer Secret.
   Callback URL (http://yourwebsite.com/twittercallback).
   TweetPic API (Get it from http://dev.twitpic.com/).
   (Note: Please set the Access Level Read & Write while creating API keys on 
   https://dev.twitter.com/).
4. Save the configuration.
5. For posting to twitter using Modules custom form go to Admin -> Structure -> 
   Blocks -> Configure (Tweet Poster) block.
